FROM python:3.8.0-slim
WORKDIR /app
ADD . /app
RUN pip install --trusted-host pypi.python.org Flask
# assign enironment NAME with value "Summer"
# ENV NAME Mark
ENV NAME Summer
CMD ["python", "app.py"]
